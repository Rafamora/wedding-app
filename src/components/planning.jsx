import React from "react";
import ExpenseForm from "./create-expense-form";
import WeddingForm from "./create-wedding-form";
import ExpenseDelete from "./deleteexpense";
import WeddingDelete from "./deletewedding";
import ExpenseTable from "./expense-table";
import ExpenseDetails from "./getexpensesbyweddingid";
import WeddingDetails from "./getweddingbyid";
import WeddingTable from "./wedding-table";

function Planning() {
  return (
    <div className="planning">
      <div class="container">
        <div class="row align-items-center my-5">
          <div class="col-lg-7">
            <img
              class="img-fluid rounded mb-4 mb-lg-0"
              src="https://photobugcommunity.com/wp-content/uploads/2018/10/how-to-create-kick-ass-contact-form-for-your-wedding-photography-website-2-700x1050.jpg" height="800" width= "500"
              alt=""
            />
          </div>
          <div class="col-lg-5">
            <h1 class="font-weight-light">Planning</h1>
            <p>
            Thank you for considering <b>R&H Wedding Planning.</b> To view, add or delete information, please use the buttons below.
            </p>
            <WeddingTable></WeddingTable>
            <hr></hr>
            <ExpenseTable></ExpenseTable>
            <hr></hr>
            <WeddingForm></WeddingForm>
            <hr></hr>
            <ExpenseForm></ExpenseForm>
            <hr></hr>
            <WeddingDetails></WeddingDetails>
            <hr></hr>
            <ExpenseDetails></ExpenseDetails>
            <hr></hr>
            <WeddingDelete></WeddingDelete>
            <hr></hr>
            <ExpenseDelete></ExpenseDelete>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Planning;