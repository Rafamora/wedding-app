import React from "react";
import MoreInfoForm from "./more-info-form";

function Chat() {
  return (
    <div className="chat">
      <div class="container">
        <div class="row align-items-center my-5">
          <div class="col-lg-7">
            <img
              class="img-fluid rounded mb-4 mb-lg-0"
              src="https://wallpaperaccess.com/full/4075151.jpg"
              alt=""
            />
          </div>
          <div class="col-lg-5">
            <h1 class="font-weight-light">Chat with our team</h1>
            <p>
            <p>To contact us, please submit your email address below and a member of our team will contact you as soon as possible.</p>
            <p>Happy Wedding Planning!</p>
            </p>
          <MoreInfoForm></MoreInfoForm>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Chat;