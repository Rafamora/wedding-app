"use strict";
exports.__esModule = true;
exports.Expense = exports.Wedding = void 0;
var Wedding = /** @class */ (function () {
    function Wedding(weddingId, weddingName, weddingDate, weddingLocation, weddingBudget) {
        this.weddingId = weddingId;
        this.weddingName = weddingName;
        this.weddingDate = weddingDate;
        this.weddingLocation = weddingLocation;
        this.weddingBudget = weddingBudget;
    }
    return Wedding;
}());
exports.Wedding = Wedding;
var Expense = /** @class */ (function () {
    function Expense(weddingId, expenseId, expenseReason, expenseAmount) {
        this.weddingId = weddingId;
        this.expenseId = expenseId;
        this.expenseReason = expenseReason;
        this.expenseAmount = expenseAmount;
    }
    return Expense;
}());
exports.Expense = Expense;
