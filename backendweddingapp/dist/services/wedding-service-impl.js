"use strict";
exports.__esModule = true;
exports.WeddingServiceImpl = void 0;
var wedding_dao_postgres_1 = require("../dao/wedding-dao-postgres");
var WeddingServiceImpl = /** @class */ (function () {
    function WeddingServiceImpl() {
        this.weddingDAO = new wedding_dao_postgres_1.WeddingDaoPostgres();
    }
    WeddingServiceImpl.prototype.updateWedding = function (wedding) {
        return this.weddingDAO.updateWedding(wedding);
    };
    WeddingServiceImpl.prototype.registerWedding = function (wedding) {
        return this.weddingDAO.createWedding(wedding);
    };
    WeddingServiceImpl.prototype.retreiveAllWeddings = function () {
        return this.weddingDAO.gettAllWeddings();
    };
    WeddingServiceImpl.prototype.retreiveWeddingById = function (weddingId) {
        return this.weddingDAO.getWeddingById(weddingId);
    };
    WeddingServiceImpl.prototype.searchByName = function (weddingName) {
        return this.weddingDAO.getWeddingById(1);
    };
    WeddingServiceImpl.prototype.removeWeddingById = function (weddingId) {
        return this.weddingDAO.deleteWeddingById(weddingId);
    };
    return WeddingServiceImpl;
}());
exports.WeddingServiceImpl = WeddingServiceImpl;
