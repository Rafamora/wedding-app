import { Expense } from "../src/entities";


export interface ExpenseDAO{
//CREATE
    createExpense(expense:Expense):Promise<Expense>;
    //createExpense

//READ
    getAllExpenses():Promise<Expense[]>;
    getExpenseById(expenseId:number):Promise<Expense>;
    getExpensesByWeddingId(weddingId:number):Promise<Expense[]>;
    //getAllExpenses
    //getExpensesById

//UPDATE
    updateExpense(expense:Expense):Promise<Expense>;
    //updateExpense

//DELETE
    deleteExpenseById(expenseId:number): Promise<boolean>
    //deleteExpenseById
    //deleteExpense

}
