import { Wedding } from "../src/entities";

export default interface WeddingService{

    registerWedding(wedding:Wedding):Promise<Wedding>;

    retreiveAllWeddings():Promise<Wedding[]>;

    retreiveWeddingById(weddingId:number):Promise<Wedding>;

    searchByName(weddingName:string):Promise<Wedding>;

    removeWeddingById(weddingId:number):Promise<boolean>;

    updateWedding(wedding:Wedding):Promise<Wedding>;


}